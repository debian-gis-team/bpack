Source: bpack
Section: python
Priority: optional
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Antonio Valentino <antonio.valentino@tiscali.it>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               dh-sequence-sphinxdoc,
               pybuild-plugin-pyproject,
               python3-all,
               python3-bitarray,
               python3-bitstruct,
               python3-numpy,
               python3-pytest <!nocheck>,
               python3-setuptools,
               python3-sphinx <!nodoc>,
               python3-sphinx-rtd-theme <!nodoc>
Standards-Version: 4.7.0
Testsuite: autopkgtest-pkg-pybuild
Rules-Requires-Root: no
Homepage: https://github.com/avalentino/bpack
Vcs-Browser: https://salsa.debian.org/debian-gis-team/bpack
Vcs-Git: https://salsa.debian.org/debian-gis-team/bpack.git
Description: Binary data structures (un-)packing library
 The *bpack* Python package provides tools to describe and encode/decode
 binary data.
 .
 Binary data are assumed to be organized in *records*, each composed by a
 sequence of fields. Fields are characterized by a known size, offset
 (w.r.t. the beginning of the record) and datatype.
 .
 The package provides classes and functions that can be used to:
 .
  * describe binary data structures in a declarative way (structures can
    be specified up to the bit level)
  * automatically generate encoders/decoders for a specified data descriptor
 .
 Encoders/decoders (*backends*) rely on well known Python packages like:
 .
  * struct (form the standard library)
  * bitstruct (optional)
  * numpy (optional)
  * bitarray (optional) - partial implementation

Package: python3-bpack
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends}
Recommends: python3-bitstruct,
            python3-numpy
Suggests: python-bpack-doc,
          python3-bitarray
Description: ${source:Synopsis}
 ${source:Extended-Description}

Package: python-bpack-doc
Section: doc
Architecture: all
Depends: ${sphinxdoc:Depends},
         ${misc:Depends}
Suggests: www-browser
Description: ${source:Synopsis} (documentation)
 ${source:Extended-Description}
 .
 This package provides documentation for bpack.
